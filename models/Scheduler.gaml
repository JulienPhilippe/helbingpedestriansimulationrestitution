/***
* Name: Scheduler
* Author: julienlitis
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model Scheduler

/* Insert your model definition here */
global
{
	bool isArrow;
	
	int lastStep;
	int currentStep <- 0;
	int id_simulation <- 1544;
	bool connect <- false;
	list<int> sim_id;
	
	int marge <- 1000;
	int maxStep <- 20000;
	
	int activation_step;
	int end_step <- 20000;
	float nbPeople <- 0.0;
	float nbNervous <- 0.0;
	int tmpPeople;
	int tmpNervous;
	float baryX;
	float baryY; 
	float allnerv;
	
	geometry shape <- rectangle(60, 7);
	
	init
	{
		create connection number:1 with:[id_simulation::id_simulation];
		
		ask connection
		{
			myself.connect <- isConnect();
		}
		
		if connect
		{
			write "Connect to Database : OK";
			list<list> walls;
			ask connection
			{
				myself.activation_step <- getActivationStep();
				walls<- getObstacle();
			}
			
			loop w over:walls
			{
				create wallSim number:1 with:[info::w];
			}
			
			ask connection
			{
				myself.sim_id <- getAgentId();
			}
			
			loop i over:sim_id
			{
				list info;
				ask connection
				{
					write "" + i + "/" + (length(myself.sim_id)-1);
					info <- getAgentInfo(i);
				}
				
				
				
				create agentSim number:1 with:[sim_id::i,infoTmp::info];
			}
		}
		else
		{
			write "Echec de la connection";
		}
		write "Fin de l'init";
		
	}
	
	reflex scheduler
	{
		if connect
		{
			nbPeople <- 0.0;
			
			tmpPeople <- 0;
			tmpNervous <- 0;
			
			float tmpBaryX <- 0.0;
			float tmpBaryY <- 0.0;
			float tmpNerv <- 0.0;
			
			if cycle > maxStep
			{
				//write "(" + (baryX/allnerv) + "," + (baryY/allnerv) + ")"; 
				do pause;
			}
			
			if cycle mod (marge) = 200 and cycle > 0
			{
				write "" + cycle + " reload";
				ask connection
				{
				myself.sim_id <- getAgentId();
				}
			
				loop i over:sim_id
				{
					create agentSim number:1 with:[sim_id::i,infoTmp::[]];
				}
				
				ask agentSim parallel:true
				{
					do setInfo;
				}
			}
			
			ask agentSim
			{
				do setLocation;
				if cycle >= activation_step
				{
					myself.nbPeople <- myself.nbPeople + int(info[cycle] != nil);
					myself.tmpPeople <- myself.tmpPeople + int(info[cycle] != nil);
					
					if float(info[cycle][2]) > 0.5
					{
						myself.nbNervous <- myself.nbNervous + 1;
						myself.tmpNervous <- myself.tmpNervous + 1;
					}
					
					tmpBaryX <- tmpBaryX + (location.x* float(info[cycle][2]));
					tmpBaryY <- tmpBaryY + (location.y* float(info[cycle][2]));
					tmpNerv <- tmpNerv + float(info[cycle][2]);
				}
			}
			if cycle > activation_step and cycle < end_step
			{
				
				
				write nbPeople;
				//write tmpPeople;
				
				write nbNervous/(cycle-activation_step);
				//if tmpNervous > 0 {write tmpNervous;}
				
				baryX <- baryX + tmpBaryX;
				baryY <- baryY + tmpBaryY;
				allnerv <- allnerv + tmpNerv;
			}
		}
	}
}


species connection skills:[SQLSKILL]
{
	map<string, string>  POSTGRES <- [
                                        'host'::'localhost',
                                        'dbtype'::'postgres',
                                        'database'::'gamaexp',
                                        'port'::'5432',
                                        'user'::'gama',
                                        'passwd'::'gama'];
                                        
    int id_simulation;
    
    action isConnect
    {
    	return testConnection(params:POSTGRES);
    }
    
    action getAgentId
    {
    	list<int> sim_id;
    	
    	list<list> t <-   list<list> (self select(params:POSTGRES, 
                         select:"select sim_id from agent where id_simulation = ? and spawntime < ? and spawntime >= ?;"
							, values:[id_simulation,(cycle+marge),(cycle)]));
							
		write 'spawntime < ' + (cycle+marge) +  'and spawntime >= ' + cycle;
		
		loop i over: t[2]
		{
			add i[0] to:sim_id;
		}		
		
		return sim_id;
    }
    
    action getAgentInfo(int agent_sim_id)
    {
    	list info;

    	list<list> t <-   list<list> (self select(params:POSTGRES, 
                         select:"select (step-200+generate_series(1,array_upper(states,1))-1) AS time,
			unnest(states[array_upper(states,1)][2:2]) as locationX, 
            unnest(states[array_upper(states,1)][3:3]) as locationY, 
            unnest(states[array_upper(states,1)][4:4]) as nervousness, 
            unnest(states[array_upper(states,1)][5:5]) as directionx, 
            unnest(states[array_upper(states,1)][6:6]) as directiony
            FROM state  WHERE states[1][1] = 1 AND id_simulation =  ? AND sim_id = ? AND step >= ? AND step < ?;"
							, values:[id_simulation,agent_sim_id,(cycle+200),(cycle+200+marge)]));
		
		loop i over: t[2]
		{
			add i to:info;
		}
		
		return info;    	
    }
    
    action getActivationStep
    {
    	list<list> t <-   list<list> (self select(params:POSTGRES, 
                         select:"select activation_step from simulation where id_simulation = ?;"
							, values:[id_simulation]));
							
		return int(t[2][0][0]);
    }
    
    action getObstacle
    {
    	list<list> t <-   list<list> (self select(params:POSTGRES, 
                         select:"SELECT coordx,coordy,largeur,longueur from wall w INNER JOIN obstacle o ON w.id_obstacle = o.id_obstacle
								INNER JOIN IncludeObstacle i ON o.id_obstacle = i.id_obstacle
								INNER JOIN configuration c ON i.id_configuration = c.id_configuration
								INNER JOIN simulation s ON s.id_configuration = c.id_configuration
								WHERE s.id_simulation = ?;"
							, values:[id_simulation]));
							
		return t[2];
    }
    
}

species agentSim
{
	int sim_id;
	map<int,list> info;
	list infoTmp;
	
	init
	{
		loop i over:infoTmp
		{
			list tmp;
			loop j from:1 to:(length(list(i))-1)
			{
				add i[j] to:tmp;
			}
			add i[0]::tmp to:info;
			
			if int(i[0]) > maxStep
			{
				maxStep <- int(i[0]);
			}
		}
		infoTmp <- [];
	}
	
	action setLocation
	{
		
		location <- {float(info[cycle][0]),float(info[cycle][1])};
	}
	
	aspect default
	{
		draw circle(0.25) color:rgb(float(info[cycle][2])*255,225-float(info[cycle][2])*255,0);
		
		if isArrow
		{
			draw line([{float(info[cycle][3]),float(info[cycle][4])},{location.x,location.y}]) color: #red begin_arrow:0.1;
		}
	}
	
	action setInfo
	{
		ask connection
		{
						//write "" + i + "/" + (length(myself.sim_id)-1);
			myself.infoTmp <- getAgentInfo(myself.sim_id);
		}
		
		loop i over:infoTmp
		{
			list tmp;
			loop j from:1 to:(length(list(i))-1)
			{
				add i[j] to:tmp;
			}
			add i[0]::tmp to:info;
			
			if int(i[0]) > maxStep
			{
				maxStep <- int(i[0]);
			}
		}
		infoTmp <- [];
	}
}

species wallSim
{
	list info;
	point size;
	
	init
	{
		
		location <- {float(info[0]),float(info[1])};
		size <- {float(info[2]),float(info[3])};
	}
	
	aspect default
	{
		draw rectangle(size) color:#black;
	}
}


